package br.com.ambientelivre.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import br.com.ambientelivre.dao.UserDAOImpl;
import br.com.ambientelivre.dao.UserDAO;
import br.com.ambientelivre.domain.User;

public class UserAction extends ActionSupport implements ModelDriven<User> {

	private static final long serialVersionUID = -6659925652584240539L;

	private User user = new User();	
	private List<User> userList = new ArrayList<User>();
	private UserDAO userDAO = new UserDAOImpl();

	
	@Override
	public User getModel() {
		return user;
	}
	
	public String saveOrUpdate()
	{	
		userDAO.saveOrUpdateUser(user);
		return SUCCESS;
	}
	
	public String list()
	{
		userList = userDAO.listUser();
		return SUCCESS;
	}
	
	public String delete()
	{
		HttpServletRequest request = (HttpServletRequest) ActionContext.getContext().get(ServletActionContext.HTTP_REQUEST);
		userDAO.deleteUser(Long.parseLong(request.getParameter("id")));
		return SUCCESS;
	}
	
	public String edit()
	{
		HttpServletRequest request = (HttpServletRequest) 
		ActionContext.getContext().get(ServletActionContext.HTTP_REQUEST);
		user = userDAO.listUserById(Long.parseLong(request.getParameter("id")));
		return SUCCESS;
	}
	
    public void validate() {
    	
    	System.out.println("TESTE AQUI VALIDA");
    	
        if (getUser().getName().length() == 0) {        	
        	addFieldError("name", "Nome do usuario é Obrigatório");
        }        
    
    	if ( getUser().getCountry().length() == 0  ) {        
    		addFieldError("country", "O pais é Obrigatório");
        } 
    }
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

	
}
