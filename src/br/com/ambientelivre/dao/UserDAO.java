package br.com.ambientelivre.dao;

import br.com.ambientelivre.domain.User;
import java.util.List;

public interface UserDAO {
	
	public void saveOrUpdateUser(User user);
	public List<User> listUser();
	public User listUserById(Long userId);
	public void deleteUser(Long userId);
	
}
